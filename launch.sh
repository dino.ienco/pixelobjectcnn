#!/bin/sh
dir="/media/DATA/AZZA/splits"

python main.py $dir/train_X_pxl_$1.npy $dir/train_X_median_$1.npy $dir/train_gt_$1.npy $dir/valid_X_pxl_$1.npy $dir/valid_X_median_$1.npy $dir/valid_gt_$1.npy $dir/test_X_pxl_$1.npy $dir/test_X_median_$1.npy $dir/test_gt_$1.npy modelTwoBranch $1
